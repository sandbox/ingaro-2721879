<?php

/**
 * @file
 * Contains \Drupal\date_range\Plugin\Field\FieldType\DateRangeItem.
 */

namespace Drupal\date_range\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'date_range' entity field type.
 *
 * @FieldType(
 *   id = "date_range",
 *   label = @Translation("Date range"),
 *   description = @Translation("An entity field containing both start/end timestamp."),
 *   default_formatter = "date_range_default",
 *   default_widget = "date_range_default"
 * )
 */
class DateRangeItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['first_date_inclusive'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(new TranslatableMarkup('First date (inclusive)'))
      ->setRequired(TRUE);

    $properties['last_date_inclusive'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(new TranslatableMarkup('Last date (inclusive)'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    // We store the dates in varchar columns, in the form "yyyy-mm-dd".
    // This probably isn't optimal, but it matches what the DateTimeItem
    // field type in core does.
    return [
      'columns' => [
        'first_date_inclusive' => [
          'description' => 'The start date.',
          'type' => 'varchar',
          'length' => 20,
        ],
        'last_date_inclusive' => [
          'description' => 'The end date.',
          'type' => 'varchar',
          'length' => 20,
        ],
      ],
      'indexes' => [
        'first_date_inclusive' => ['first_date_inclusive'],
        'last_date_inclusive' => ['last_date_inclusive'],
      ],
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->first_date_inclusive) && empty($this->last_date_inclusive);
  }

}
