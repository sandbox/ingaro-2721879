<?php

/**
 * @file
 * Contains \Drupal\date_range\Plugin\Field\FieldFormatter\DateRangeFormatter.
 */

namespace Drupal\date_range\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Plugin implementation of the 'default' formatter for 'date_range' fields.
 *
 * @FieldFormatter(
 *   id = "date_range_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "date_range"
 *   }
 * )
 */
class DateRangeFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The date format entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $dateFormatStorage;

  /**
   * Constructs a new DateRangeFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $date_format_storage
   *   The date format storage.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, DateFormatterInterface $date_formatter, EntityStorageInterface $date_format_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->dateFormatter = $date_formatter;
    $this->dateFormatStorage = $date_format_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('date.formatter'),
      $container->get('entity.manager')->getStorage('date_format')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'date_format' => 'medium',
      'custom_date_format' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $date_formats = array();

    foreach ($this->dateFormatStorage->loadMultiple() as $machine_name => $value) {
      $date_formats[$machine_name] = $this->t('@name format: @date', ['@name' => $value->label(), '@date' => $this->dateFormatter->format(REQUEST_TIME, $machine_name)]);
    }

    $date_formats['custom'] = $this->t('Custom');

    $elements['message'] = [
      '#type' => 'item',
      '#description' => $this->t('Please note: it does not make sense to use date formats with a time component here.'),
    ];

    // TODO: need a message instructing the site builder that using a date format with time components does not make sense
    $elements['date_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Date format'),
      '#options' => $date_formats,
      '#default_value' => $this->getSetting('date_format') ?: 'medium',
    ];

    // TODO: need a message instructing the site builder that using a date format with time components does not make sense
    $elements['custom_date_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom date format'),
      '#description' => $this->t('See <a href=":url" target="_blank">the documentation for PHP date formats</a>.', [':url' => 'http://php.net/manual/function.date.php']),
      '#default_value' => $this->getSetting('custom_date_format') ?: '',
    ];

    $elements['custom_date_format']['#states']['visible'][] = [
      ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][date_format]"]' => array('value' => 'custom'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $date_format = $this->getSetting('date_format');
    $summary[] = $this->t('Date format: @date_format', ['@date_format' => $date_format]);
    if ($this->getSetting('date_format') === 'custom' && ($custom_date_format = $this->getSetting('custom_date_format'))) {
      $summary[] = $this->t('Custom date format: @custom_date_format', ['@custom_date_format' => $custom_date_format]);
    }

    return $summary;
  }


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $date_format = $this->getSetting('date_format');
    $custom_date_format = $this->getSetting('custom_date_format');
    $timezone = $this->getSetting('timezone') ?: NULL;
    $langcode = NULL;

    // If an RFC2822 date format is requested, then the month and day have to
    // be in English. @see http://www.faqs.org/rfcs/rfc2822.html
    if ($date_format === 'custom' && $custom_date_format === 'r') {
      $langcode = 'en';
    }

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#cache' => [
          'contexts' => [
            'timezone',
          ],
        ],
        '#markup' => $this->t('@first to @last', [
          '@first' => $this->formatValue($item->first_date_inclusive, $date_format, $custom_date_format, $timezone, $langcode),
          '@last' => $this->formatValue($item->last_date_inclusive, $date_format, $custom_date_format, $timezone, $langcode),
        ]),
      ];
    }

    return $elements;
  }



  /**
   * @param string|NULL $value
   * @param string $date_format
   * @param string|NULL $custom_date_format
   * @param string|NULL $timezone
   * @param string|NULL $langcode
   * @return string|NULL
   */
  private function formatValue($value, $date_format, $custom_date_format, $timezone, $langcode) {

    // turn the value into a DrupalDateTime object
    // this involves adding in the missing time elements (midnight)
    // and setting a timezone

    $date_time = $timezone ? \DateTime::createFromFormat('Y-m-d', $value, new \DateTimeZone($timezone)) : \DateTime::createFromFormat('Y-m-d', $value);
    $date_time->setTime(0, 0, 0);
    if ($date_time) {
      return $this->dateFormatter->format($date_time->getTimestamp(), $date_format, $custom_date_format, $timezone, $langcode);
    }
    return NULL;
  }

}
