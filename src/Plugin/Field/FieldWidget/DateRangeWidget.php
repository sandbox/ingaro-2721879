<?php

namespace Drupal\date_range\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'date_range_default' widget.
 *
 * @FieldWidget(
 *   id = "date_range_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "date_range"
 *   }
 * )
 */
class DateRangeWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // We are nesting some sub-elements inside the parent, so we need a wrapper.
    // We also need to add another #title attribute at the top level for ease in
    // identifying this item in error messages. We do not want to display this
    // title because the actual title display is handled at a higher level by
    // the Field module.

    $element['#theme_wrappers'][] = 'datetime_wrapper';
    $element['#attributes']['class'][] = 'container-inline';

    $element['first_date_inclusive'] = array(
      '#type' => 'date',
      '#title' => $this->t('From'),
      '#default_value' => $items[$delta]->first_date_inclusive,
      '#date_increment' => 1,
      '#date_timezone' => drupal_get_user_timezone(),
      '#required' => $element['#required'],
    );

    $element['last_date_inclusive'] = array(
      '#type' => 'date',
      '#title' => $this->t('To'),
      '#default_value' => $items[$delta]->last_date_inclusive,
      '#date_increment' => 1,
      '#date_timezone' => drupal_get_user_timezone(),
      '#required' => $element['#required'],
    );

    return $element;
  }

}
