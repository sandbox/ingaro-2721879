<?php

namespace Drupal\date_range\Plugin\views\argument;

use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Drupal\views\Plugin\views\argument\Date;

/**
 * Basic argument handler for arguments that are numeric. Incorporates
 * break_phrase.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("date_range")
 */
class DateRangeArgumentPlugin extends Date {


  /**
   * Set up the query for this argument.
   *
   * The argument sent may be found at $this->argument.
   */
  public function query($group_by = FALSE) {

    $first_date_field = "{$this->tableAlias}.{$this->realField}_first_date_inclusive";
    $last_date_field = "{$this->tableAlias}.{$this->realField}_last_date_inclusive";

    $this->ensureMyTable();
    $this->query->addWhere(0, $first_date_field, $this->argument, '<=');
    $this->query->addWhere(0, $last_date_field, $this->argument, '>=');
  }


}
