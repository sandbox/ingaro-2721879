<?php

use Drupal\field\FieldStorageConfigInterface;


/**
 * Implements hook_field_views_data().
 */
function date_range_field_views_data(FieldStorageConfigInterface $field_storage) {
  // @todo This code only covers configurable fields, handle base table fields
  //   in https://www.drupal.org/node/2489476.

  $data = views_field_default_views_data($field_storage);

  foreach ($data as $table_name => $table_data) {

    // Set the 'datetime' filter type.
    $data[$table_name][$field_storage->getName() . '_first_date_inclusive']['filter']['id'] = 'datetime';
    $data[$table_name][$field_storage->getName() . '_last_date_inclusive']['filter']['id'] = 'datetime';

    // Set the 'datetime' argument type.
    $data[$table_name][$field_storage->getName() . '_first_date_inclusive']['argument']['id'] = 'datetime';
    $data[$table_name][$field_storage->getName() . '_last_date_inclusive']['argument']['id'] = 'datetime';

    $data[$table_name][$field_storage->getName()]['argument'] = [
      'id' => 'date_range',
    ];

    // Set the 'datetime' sort handler.
    $data[$table_name][$field_storage->getName() . '_first_date_inclusive']['sort']['id'] = 'datetime';
    $data[$table_name][$field_storage->getName() . '_last_date_inclusive']['sort']['id'] = 'datetime';
  }

  return $data;
}

